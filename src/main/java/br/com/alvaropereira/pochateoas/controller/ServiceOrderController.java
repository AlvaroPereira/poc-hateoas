package br.com.alvaropereira.pochateoas.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import br.com.alvaropereira.pochateoas.model.ServiceOrder;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ServiceOrderController {

    private List<ServiceOrder> serviceOrders = new ArrayList<>();

    public ServiceOrderController() {
        serviceOrders.add(new ServiceOrder(1L, "Order 1", "Customer A", LocalDateTime.now()));
        serviceOrders.add(new ServiceOrder(2L, "Order 2", "Customer B", LocalDateTime.now()));
        serviceOrders.add(new ServiceOrder(3L, "Order 3", "Customer C", LocalDateTime.now()));
    }

    @GetMapping("/service-orders")
    public ResponseEntity<CollectionModel<EntityModel<ServiceOrder>>> getAllServiceOrders() {
        List<EntityModel<ServiceOrder>> serviceOrderList = new ArrayList<>();
        for (ServiceOrder serviceOrder : serviceOrders) {
            serviceOrderList.add(getServiceOrderEntityModel(serviceOrder));
        }
        Link selfLink = WebMvcLinkBuilder.linkTo(ServiceOrderController.class).withSelfRel();
        CollectionModel<EntityModel<ServiceOrder>> serviceOrderCollection = CollectionModel.of(serviceOrderList, selfLink);
        return ResponseEntity.ok(serviceOrderCollection);
    }

    @GetMapping("/service-orders/{id}")
    public ResponseEntity<EntityModel<ServiceOrder>> getServiceOrderById(@PathVariable Long id) {
        ServiceOrder serviceOrder = getServiceOrderByIdFromList(id);
        if (serviceOrder == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(getServiceOrderEntityModel(serviceOrder));
    }

    @PostMapping("/service-orders")
    public ResponseEntity<EntityModel<ServiceOrder>> createServiceOrder(@RequestBody ServiceOrder serviceOrder) {
        serviceOrder.setId(serviceOrders.size() + 1L);
        serviceOrder.setCreatedAt(LocalDateTime.now());
        serviceOrders.add(serviceOrder);
        EntityModel<ServiceOrder> serviceOrderEntityModel = getServiceOrderEntityModel(serviceOrder);
        Link selfLink = serviceOrderEntityModel.getLink(IanaLinkRelations.SELF).orElse(null);
        return ResponseEntity.created(URI.create(selfLink.getHref())).body(serviceOrderEntityModel);
    }

    @PutMapping("/service-orders/{id}")
    public ResponseEntity<EntityModel<ServiceOrder>> updateServiceOrder(@PathVariable Long id, @RequestBody ServiceOrder updatedServiceOrder) {
        ServiceOrder serviceOrder = getServiceOrderByIdFromList(id);
        if (serviceOrder == null) {
            return ResponseEntity.notFound().build();
        }
        serviceOrder.setDescription(updatedServiceOrder.getDescription());
        serviceOrder.setCustomer(updatedServiceOrder.getCustomer());
        EntityModel<ServiceOrder> serviceOrderEntityModel = getServiceOrderEntityModel(serviceOrder);
        Link selfLink = serviceOrderEntityModel.getLink(IanaLinkRelations.SELF).orElse(null);
        assert selfLink != null;
        return ResponseEntity.ok().location(URI.create(selfLink.getHref())).body(serviceOrderEntityModel);
    }

    @DeleteMapping("/service-orders/{id}")
    public ResponseEntity<Void> deleteServiceOrder(@PathVariable Long id) {
        ServiceOrder serviceOrder = getServiceOrderByIdFromList(id);
        if (serviceOrder == null) {
            return ResponseEntity.notFound().build();
        }
        serviceOrders.remove(serviceOrder);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    private ServiceOrder getServiceOrderByIdFromList(Long id) {
        for (ServiceOrder serviceOrder : serviceOrders) {
            if (serviceOrder.getId().equals(id)) {
                return serviceOrder;
            }
        }
        return null;
    }

    private EntityModel<ServiceOrder> getServiceOrderEntityModel(ServiceOrder serviceOrder) {
        Link selfLink = WebMvcLinkBuilder.linkTo(ServiceOrderController.class).slash(serviceOrder.getId()).withSelfRel();
        Link allServiceOrdersLink = WebMvcLinkBuilder.linkTo(ServiceOrderController.class).withRel("allServiceOrders");
        return EntityModel.of(serviceOrder, selfLink, allServiceOrdersLink);
    }
}