package br.com.alvaropereira.pochateoas.model;

import java.time.LocalDateTime;

public class ServiceOrder {
    private Long id;
    private String description;
    private String customer;
    private LocalDateTime createdAt;

    public ServiceOrder(Long id, String description, String customer, LocalDateTime createdAt) {
        this.id = id;
        this.description = description;
        this.customer = customer;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
