package br.com.alvaropereira.pochateoas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PochateoasApplication {

    public static void main(String[] args) {
        SpringApplication.run(PochateoasApplication.class, args);
    }

}
